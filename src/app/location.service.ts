import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private geoLocation: Geolocation) { }

  public getLocation() {
    return this.geoLocation.getCurrentPosition();
  }
}
